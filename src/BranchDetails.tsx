import { StyleSheet, View, Text } from 'react-native';
import { Branch } from './Branch';

export default function BranchDetails({ branch }: { branch: Branch }) {
  
  const accessibilityDescription = () => {
   const description = branch.Accessibility.reduce((finalString, item, index, sourceArr) => {
      // TODO: error checking for other format of description 
      if (sourceArr.length == 1) {
        // we don't have comma and appending and to last item in such case
        const splitItem = item.match(/[A-Z][a-z]+/g);
        return `${finalString}${getUserReadableString(splitItem, index != 0)}`
      }

      const splitItem = item.match(/[A-Z][a-z]+/g);
      const lastItem = index == sourceArr.length - 1
      const secondLastItem = index == sourceArr.length - 2
      const comma = ', ' // join all items with a space except the last and second last item
      const and = ' and ' // add last item with and to denote end of items list
      return `${finalString}${lastItem ? and : ''}${getUserReadableString(splitItem, index != 0)}${(!lastItem && !secondLastItem)? comma: ''}`
   }, "")
   return description
  }

  // return [This, Is, Test] => "This is test"
  // param - sourceStringArr - Array of strings 
  // shouldMakeAllLowerCase - Boolean - pass true in case you want to convert
  // [This, Is, Test] => this is test
  const getUserReadableString = (sourceStringArr: [String], shouldMakeAllLowerCase: Boolean) => {
    if (sourceStringArr == null)
      return ""

    // make final string and convert each word after first to be lowercase
    return sourceStringArr.reduce((finalString, currentItem, index) => {
       const convertToLowerCase = (index != 0 || shouldMakeAllLowerCase)
       const lastItem = index == sourceStringArr.length - 1
       const space = ' ' // join all items with a space except the last item
       return `${finalString}${convertToLowerCase ? currentItem.toLowerCase() : currentItem}${!lastItem ? space: ''}`;              
    }, "") 
  }

  return (
    <View style={styles.container}>
      <View style={styles.row}>
        <Text style={styles.text}>Branch name:</Text>
        <Text style={styles.textBold}>{branch.Name}</Text>
      </View>
      {branch.ServiceAndFacility && (
        <View style={styles.row}>
          <Text style={styles.text}>Services:</Text>
          <Text style={styles.textBold}>
            {branch.ServiceAndFacility.join(', ')}
          </Text>
        </View>
      )}
      {branch.Accessibility && (
        <View style={styles.row}>
          <Text style={styles.text}>Accessibility:</Text>
          <Text style={styles.textBold}>{accessibilityDescription()}</Text>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#80808030',
    padding: 10,
    marginHorizontal: 20,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    fontFamily: 'textRegular',
    color: 'black',
    fontSize: 14,
    flex: 1,
  },
  textBold: {
    fontFamily: 'textBold',
    color: 'black',
    fontSize: 14,
    flex: 1,
  },
});
